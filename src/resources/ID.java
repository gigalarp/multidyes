package resources;

//Different IDs
public class ID {

    public final static int ID_AGGIE = 922;

    //NOTE: These ids apply to every basic door!
    public final static int ID_DOOR_CLOSED = 1239;
    public final static int ID_DOOR_OPENED = 1240;

    public final static int ID_WOAD_LEAF = 1793;
    public final static int ID_REDBERRIES = 1951;
    public final static int ID_ONION = 1957;
    public final static int ID_MONEY = 995;

    public final static int ID_BLUE_DYE = 1767;
    public final static int ID_RED_DYE = 1763;
    public final static int ID_YELLOW_DYE = 1765;

    public final static int[] ID_BANKERS = {4459, 4457, 4458, 4456}; //these are in order (from left to right)
    public final static int[] ID_BANK_BOOTHS = {2012, 2015, 2019};

}
