package script;


import API.Dye;
import debug.Logger;
import org.powerbot.script.rt6.ClientContext;
import resources.ID;

public class Config {

    private int dyeId;
    private int resourceId;
    private int resourcesPerDye;

    private ClientContext ctx;
    private Dye dye;


    public Config(ClientContext ctx) {
        this.ctx = ctx;
    }

    public void setBlueDye() {
        this.dyeId = ID.ID_BLUE_DYE;
        this.resourceId = ID.ID_WOAD_LEAF;
        this.resourcesPerDye = 2;

        this.dye = new Dye() {
            @Override
            public int howManyDyesCanWeMake() { //TODO: Works but still needs to be tested thoroughly
                Logger.log("Config.java - howManyDyesCanWeMake (Blue ones):");

                Logger.log("\tHow many dyes can we make:");
                if(ctx.backpack.select().id(ID.ID_WOAD_LEAF).count() != 0)
                    if(ctx.backpack.select().id(ID.ID_WOAD_LEAF).peek().stackSize() >= 2) //If we have two or more woad leaves
                        if((28 - ctx.backpack.select().count()) > (ctx.backpack.select().id(ID.ID_WOAD_LEAF).peek().stackSize() / 2)) { //If we have more inventory space than the potential dye amount made from our resources
                            Logger.log("\t\t1st return value: " + (ctx.backpack.select().id(ID.ID_WOAD_LEAF).peek().stackSize() / 2));
                            return (ctx.backpack.select().id(ID.ID_WOAD_LEAF).peek().stackSize() / 2);
                        } else {//TODO: TEST
                            Logger.log("\t\t2nd return value: " + (28 - ctx.backpack.select().count()));
                            return (28 - ctx.backpack.select().count()); //Return number of empty space
                        }

                Logger.log("\t\t3rd return value: 0");
                return 0;
            }
        };
    }

    public void setRedDye() {
        this.dyeId = ID.ID_RED_DYE;
        this.resourceId = ID.ID_REDBERRIES;
        this.resourcesPerDye = 3;

        this.dye = new Dye() {
            @Override
            public int howManyDyesCanWeMake() { //TODO: TEST THIS!
                return (ctx.backpack.select().id(ID.ID_REDBERRIES).count() / 3);
            }
        };
    }

    public void setYellowDye() {
        this.dyeId = ID.ID_YELLOW_DYE;
        this.resourceId = ID.ID_ONION;
        this.resourcesPerDye = 2;

        this.dye = new Dye() {
            @Override
            public int howManyDyesCanWeMake() { //TODO: TEST THIS!

                if(ctx.backpack.moneyPouchCount() < (14 * 5))
                    if((ctx.backpack.moneyPouchCount() / 5) < (ctx.backpack.select().id(ID.ID_ONION).count() / 2))
                        return (ctx.backpack.moneyPouchCount() / 5);

                return (ctx.backpack.select().id(ID.ID_ONION).count() / 2);
            }
        };
    }

    //Returns current dye ID
    public int getDyeId() {
        return this.dyeId;
    }

    //Returns current resource ID
    public int getResourceId() {
        return this.resourceId;
    }


    //TODO: Move these somewhere else (State?)
    //Returns the amount of dyes we can make with our resources in backpack
    public int howManyDyesCanWeMake() {
        return this.dye.howManyDyesCanWeMake();
    }

    public int getResourcesPerDye() {
        return this.resourcesPerDye;
    }

}
