package API;

import debug.Logger;
import org.powerbot.script.Condition;
import org.powerbot.script.Random;
import org.powerbot.script.rt6.ClientContext;
import org.powerbot.script.rt6.Component;
import org.powerbot.script.rt6.Npc;
import resources.ID;
import script.Config;
import script.State;

import java.util.concurrent.Callable;

public class MakeDye extends Task<ClientContext>{


    private final State state;
    private final Component selectionWidget;
    private final String shortcut;
    private final Config config;

    private java.util.Random random;

    private int dye_counter;

    private final int chance_mouseOverKeyboard1; //chance that we use mouse instead of using keyboard input (In chat)
    private final int chance_mouseOverKeyboard2;
    private final int chance_mouseOverKeyboard3;
    private final int chance_finishing_conversation; //for good antiban this value should increase a little when runtime increases but not too perfectly (emulates human fatigue)
    private final int chance_not_wait;

    private boolean last_failed;


    public MakeDye(ClientContext ctx, State state, Component selectionWidget, String shortcut, Config config) {
        super(ctx);

        this.state = state;
        //Set the right selection widget and the right shortcut associated with it.
        this.selectionWidget = selectionWidget;
        this.shortcut = shortcut;
        this.config = config;

        this.random = new java.util.Random();

        dye_counter = 0;
        last_failed = false;

        //Init antiban
        //NOTE: These are not 'humanly random' and therefore we probably make patterns that humans don't do
        //NOTE2: Chance must be over 1 (nextInt will throw an exception with param value of 0)
        chance_mouseOverKeyboard1 = random.nextInt(5) + 20;
        chance_mouseOverKeyboard2 = (random.nextInt(chance_mouseOverKeyboard1) % (random.nextInt(3) + 1))+ random.nextInt(2) + 18;
        chance_mouseOverKeyboard3 = random.nextInt(4) + random.nextInt(5) + 15;
        chance_finishing_conversation = random.nextInt(5) + random.nextInt(random.nextInt(3)) + 5;
        chance_not_wait = random.nextInt(5) + 10;

        Logger.log("[INIT] chance_mouseOverKeyboard1 = " + chance_mouseOverKeyboard1);
        Logger.log("[INIT] chance_mouseOverKeyboard1 = " + chance_mouseOverKeyboard2);
        Logger.log("[INIT] chance_mouseOverKeyboard1 = " + chance_mouseOverKeyboard3);
        Logger.log("[INIT] chance_finishing_conversation = " + chance_finishing_conversation);
        Logger.log("[INIT] chance_not_wait = " + chance_not_wait);

        Logger.log("");

    }

    public boolean activate() {
        return (((ctx.backpack.moneyPouchCount() >= 5) && (this.config.howManyDyesCanWeMake() != 0)) && (this.state.areWeInsideAggiesHouse() || (this.state.isAggiesDoorOpen() && (this.state.areWeAtAggiesFrontYard() ||  ctx.npcs.select().id(ID.ID_AGGIE).peek().inViewport()))));
    }

    public void execute() {

        Npc aggie = ctx.npcs.select().id(ID.ID_AGGIE).peek();
        //int dyes_in_beginning = ctx.backpack.select().id(this.config.getDyeId()).count();

        //Set camera pitch back up if needed
        AntiBan.setCameraPitchUp(ctx);

        //Start interaction with Aggie
        if(last_failed /*|| (ctx.backpack.select().id(this.config.getDyeId()).count() == 0)*/) {
            last_failed = false;
            ctx.camera.turnTo(aggie);
        }
        aggie.interact("Make-dyes");


        //wait until the dye choosing widget comes up
        Condition.wait(new Callable<Boolean>() {
            @Override
            public Boolean call() {

                //check if widget 1188 is present currently
                if (ctx.widgets.widget(1188).valid() && ctx.chat.chatting()) {
                    //    log("It indeed is.");
                    return true;
                }

                return false;
            }
        }, Random.getDelay(), 20);

        //If we're not chatting, return
        if(!ctx.chat.chatting() || !ctx.widgets.widget(1188).valid()) {
            Logger.log("Either we're not chatting or we haven't gotten the next chat widget. (1)");
            last_failed = true;
            return;
        }

        //Select the wanted dye
        if (this.random.nextInt(chance_mouseOverKeyboard1) == 0) //use mouse
            this.selectionWidget.click();
        else //use default
            ctx.input.send(this.shortcut);


        //Wait for the next widget
        Condition.wait(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return (ctx.widgets.widget(1191).valid() && ctx.chat.chatting());
            }
        }, Random.getDelay(), 20);

        //If we're not chatting, return
        if(!ctx.chat.chatting() || !ctx.widgets.widget(1191).valid()) {
            Logger.log("Either we're not chatting or we haven't gotten the next chat widget. (2)");
            last_failed = true;
            return;
        }

        //How many dyes can we make (before making this current)
        final int howManyCanWeMake = this.config.howManyDyesCanWeMake();
        Logger.log("[DEBUG] We can make this many dyes: " + howManyCanWeMake);

        //Buy the dye
        if(this.random.nextInt(this.chance_mouseOverKeyboard2) == 0)
            ctx.widgets.component(1191, 12).click();
        else
            ctx.input.send(" ");


        //AntiBan: sometimes skip waiting (results in clicking Aggie too early)
        //Make sure that we don't skip waiting for the last dye (causes to make one too much and to drop it to the ground)
        if((random.nextInt(this.chance_not_wait) != 0) || (howManyCanWeMake == 1))
            Condition.wait(new Callable<Boolean>() {
                @Override
                public Boolean call() {
                    //Wait until a dye pops in our inventory
                    return (howManyCanWeMake != config.howManyDyesCanWeMake());
                }
            }, Random.getDelay(), 20);

        //Close the last widget depending on the chance
        if(this.random.nextInt(this.chance_finishing_conversation) == 0) {
            Logger.log("[DEBUG] We are going to finish the conversation.");
            if(random.nextInt(this.chance_mouseOverKeyboard3) == 0)
                ctx.widgets.component(1189, 16).click();
            else
                ctx.input.send(" ");
            //Notice: We don't wait here for the dialog to finish, because it would slow things down. This is merely a spamclick.
        }

        this.dye_counter++;
    }


    @Override
    public String name() {
        return "Making dyes";
    }

    public int getDyeCounter() {
        return this.dye_counter;
    }

}
