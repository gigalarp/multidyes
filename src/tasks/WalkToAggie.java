package tasks;

import API.Activation;
import API.AntiBan;
import API.Door;
import API.Task;
import debug.Logger;
import org.powerbot.script.Condition;
import org.powerbot.script.rt6.ClientContext;
import resources.ID;
import script.Config;
import script.State;

import java.util.concurrent.Callable;

public class WalkToAggie extends Task<ClientContext> {

    private State state;
    Activation activator; //Wrapper object for state activation


    public WalkToAggie(ClientContext ctx, Door aggiesDoor, Config config) {
        super(ctx);

        state = new State(ctx, aggiesDoor);

        if(config.getDyeId() == ID.ID_BLUE_DYE) {
            final ClientContext ctxRef = ctx;
            this.activator = new Activation() {
                @Override
                public boolean activate() {
                    return (state.areWeInTheBank() && (ctxRef.backpack.select().id(ID.ID_WOAD_LEAF).count() > 0) && (ctxRef.backpack.select().count() == 1) && (ctxRef.backpack.moneyPouchCount() > 5));
                }
            };
        }else if(config.getDyeId() == ID.ID_RED_DYE) {
            final ClientContext ctxRef = ctx;
            this.activator = new Activation() {
                @Override
                public boolean activate() {
                    return (state.areWeInTheBank() && (ctxRef.backpack.select().id(ID.ID_REDBERRIES).count() >= 3) && (ctxRef.backpack.moneyPouchCount() > 5)); //TODO: TEST
                }
            };
        }else if(config.getDyeId() == ID.ID_YELLOW_DYE) {
            final ClientContext ctxRef = ctx;
            this.activator = new Activation() {
                @Override
                public boolean activate() {
                    return (state.areWeInTheBank() && (ctxRef.backpack.select().id(ID.ID_ONION).count() >= 2) && (ctxRef.backpack.moneyPouchCount() > 5)); //TODO: TEST
                }
            };
        }else {
            Logger.fatal("[FATAL ERROR] in WalkToAggie.java while trying to select right dye. Will exit now.", ctx);
        }


    }

    @Override
    public boolean activate() {
        return this.activator.activate();
    }

    @Override
    public void execute() {

        //Run straight to her house
        if(state.isAggiesDoorOpen()) {

            ctx.movement.step(AntiBan.getTileToAggiesHouse());

            //If we misclicked and the door is open
            if(!state.isDestinationAtAggiesHouse() && state.isAggiesDoorOpen()) {
                Condition.wait(new Callable<Boolean>() {
                    @Override
                    public Boolean call() {
                        //return, because the door has closed
                        if(!state.isAggiesDoorOpen())
                            return true;

                        //Is destination at Aggie's house
                        if(state.isDestinationAtAggiesHouse())
                            return true;

                        ctx.movement.step(AntiBan.getTileToAggiesHouse());

                        return false;
                    }
                }); //Default delay is acceptable.
            }

            //Door could also close here (We need react fast to it)
            if(state.isAggiesDoorOpen()) {
                //Wait until the door closes OR when we are inside the house of hers
                Condition.wait(new Callable<Boolean>() {
                    @Override
                    public Boolean call() {
                        return (!state.isAggiesDoorOpen() || state.areWeInsideAggiesHouse() || (state.isAggiesDoorOpen() && state.areWeAtAggiesFrontYard()));
                    }
                }); //Is default delay long enough for border cases?
            }

        }
        //If we aren't yet in Aggie's house and the door is closed
        if(!state.isAggiesDoorOpen() && !state.areWeInsideAggiesHouse()) {
            ctx.movement.step(AntiBan.getTileToAggiesFrontYard(ctx));

            //Wait until one of the Tiles is clicked
            if(!state.isDestinationAtAggiesFrontYard()) {
                Condition.wait(new Callable<Boolean>() {
                    @Override
                    public Boolean call() {
                        if(state.isDestinationAtAggiesFrontYard())
                            return true;

                        ctx.movement.step(AntiBan.getTileToAggiesFrontYard(ctx));

                        return false;
                    }
                }); //Default delay is acceptable.
            }

            //Wait until we reach the Tile at Aggie's front yard
            Condition.wait(new Callable<Boolean>() {
                @Override
                public Boolean call() {
                    return state.areWeAtAggiesFrontYard();
                }
            }); //Default delay is acceptable.

        }

    }

    @Override
    public String name() {
        return "Walking to Aggie";
    }

}
