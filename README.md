# MultiDyes
MultiDyes WAS a RS3 bot for RSBot which made either blue, red or yellow dyes in draynor village for nice profits.
Due to powerbot being terminated the bot won't be any use.

### Links
Video showcasing the script:
https://www.youtube.com/watch?v=myQvAjmA9j4

## Features
  * Simple design
  * If the bot gets stuck, it will shortly return from the task thus making the bot run pretty smoothly
  * Basic AntiBan/AntiPattern
  * A grapical menu to choose the dye color
  * A nice overlay paint showing the stats for current run

## Requirements
  Java:

  * OpenJDK8
  * OpenJRE8

## Usage
Linux:
1. chmod +x run.sh
2. ./run.sh (./run.sh --help will show options for the run script)

Windows: (I have NOT tested these .bat files, if any problems occur with them please contact me in the forum thread)
1. Download current RSBot and move it in this directory. Then rename it to 'RSBot.jar'
2. Compile the script if you havent already or if you have modified the sources by running compile.bat file
3. Run the script by running run.bat


  * Have enough resources in bank and have enough money in bank or in the money pouch. The script will quit at the bank once it's determined it can't make any dyes or it has less money than what is needed to make one run of dyes.
  * Start at the bank or inside Aggie's house if you have resources and money already in your backpack.


## Limitations
This bot was designed to run in legacy mode and it hasn't been tested in eoc mode. It probably works there too, but the banner will probably be misaligned.


## Issues
The bot has been tested pretty thoroughly, but if you find some bugs that makes the bot unusable please post information about the bug on the powerbot forum.

Doesn't show the profit and profit per hour since I haven't found a good place (runescape's website isn't an option since it would create an obvious pattern) to lookup the item prices.

## License
Please consult the LICENSE file
